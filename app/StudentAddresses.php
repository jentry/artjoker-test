<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Students;

class StudentAddresses extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_address';

    protected $fillable = ["houseNo", "line_1", "line_2", "postcode", "city"];

    public function student()
    {
        return $this->belongsTo('App\Students');
    }
}
