<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
use App\Course;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course')->get();
		
        return view('view_students', compact(['students']));
    }

    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV(Request $request)
    {
        $students_ids =  explode(',',$request->students);
        $students = Students::with('course')->find($students_ids)->toArray();

		$fp = fopen(public_path().'/students.csv', 'w');

        foreach ($students as $file) {
			$result = [];
			array_walk_recursive($file, function($item) use (&$result) {
				$result[] = $item;
			});
			fputcsv($fp, $result);
		}
		

        fclose($fp);

        echo json_encode(['file' => '/students.csv']);
    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exportCourseAttendenceToCSV()
    {

    }
}
