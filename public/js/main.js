document.addEventListener('DOMContentLoaded', function(){

    var _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    document.getElementById('select_all').addEventListener('click', function(){
       var checkboxes = Array.from(document.querySelectorAll('input[name="studentId"]'));

        if(this.classList.contains('clicked')){
           checkboxes.forEach(function(one){
               one.checked = false;
           });
           this.classList.remove('clicked');
           this.value = 'Select all';
       }
       else{
            checkboxes.forEach(function(one){
                one.checked = true;
            });
            this.classList.add('clicked');
            this.value = 'Clear selecting';
       }
    });


    document.getElementById('export').addEventListener('click', function(){
        var checkboxes = Array.from(document.querySelectorAll('input[name="studentId"]:checked')),
            students = [];
        checkboxes.forEach(function(one){
           students.push(one.value);
        });

        if(students.length){
            students = "?students="+students.join(',');

            fetch('/export/'+students,{
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            }).then(function(response) {
                return response.json()
            }).then(function(body) {
				window.location.href = body.file;
            });
        }


    });
});